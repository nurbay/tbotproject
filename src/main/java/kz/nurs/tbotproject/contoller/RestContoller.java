package kz.nurs.tbotproject.contoller;


import kz.nurs.tbotproject.bot.WebhookBot;
import kz.nurs.tbotproject.entity.Chat;
import kz.nurs.tbotproject.util.KeyBoardManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.HashMap;

@RestController
public class RestContoller {

    final String selectOperatorString = "Select arithmetic operator";
    final String enterFirstNumberString = "Enter first number";
    final String enterSecondNumberString = "Enter second number";
    private HashMap<Long, Chat> chats = new HashMap<>();

    @Autowired
    private WebhookBot bot;

    @PostMapping(path = "/1494997160:AAHM5XibPZvW59SwhpYEUnYU1swd7TDucXE")
    public @ResponseBody
    String indexPost(@RequestBody Update update) {
        SendMessage sendMessage = new SendMessage();

        if (update.hasMessage() && update.getMessage().hasText())
            handleUpdate(update, sendMessage);

        else if (update.hasCallbackQuery()) {

            String callbackData = update.getCallbackQuery().getData();
            Long chatId = update.getCallbackQuery().getMessage().getChatId();
            System.out.println(String.format("Chat [%d], callback data: [%s]", chatId, callbackData));

            if (ifOperatorSelected(callbackData)) {
                handleOperatorSelected(callbackData, chatId, sendMessage);
            } else if (callbackData.startsWith(KeyBoardManager.Operand.FirstOperand.toString())) {
                handleFirstOperandSelected(callbackData, chatId, sendMessage);
            } else if (callbackData.startsWith(KeyBoardManager.Operand.SecondOperand.toString())) {
                handleSecondOperandSelected(callbackData, chatId, sendMessage);
            }
        }

        return "ok";
    }

    private Boolean ifOperatorSelected(String callbackData) {
        return callbackData.matches("[-+*/]");
    }

    private void handleUpdate(Update update, SendMessage sendMessage) {
        System.out.println("update: " + update.getMessage().getText());

        Chat c = new Chat();
        Long chatId = update.getMessage().getChatId();
        c.setChatId(chatId);

        sendMessage.setText(selectOperatorString);
        sendMessage.setReplyMarkup(KeyBoardManager.getOperatorsKeyboard());
        sendMessage.setChatId(chatId);

        chats.put(chatId, c);

        try {
            bot.execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    private void handleOperatorSelected(String callbackData, Long chatId, SendMessage sendMessage) {
        sendMessage.setReplyMarkup(KeyBoardManager.getNumbersKeyboard(KeyBoardManager.Operand.FirstOperand));
        sendMessage.setText(enterFirstNumberString);
        sendMessage.setChatId(chatId);

        Chat chat = chats.get(chatId);
        if (chat != null) {
            chat.setOperator(callbackData);

            try {
                bot.execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleFirstOperandSelected(String callbackData, Long chatId, SendMessage sendMessage) {
        Integer firstOperand = Integer.parseInt(callbackData.substring(callbackData.length() - 1));

        sendMessage.setReplyMarkup(KeyBoardManager.getNumbersKeyboard(KeyBoardManager.Operand.SecondOperand));
        sendMessage.setText(enterSecondNumberString);
        sendMessage.setChatId(chatId);

        Chat chat = chats.get(chatId);
        if (chat != null) {
            chat.setFirstOperand(firstOperand);
            try {
                bot.execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleSecondOperandSelected(String callbackData, Long chatId, SendMessage sendMessage) {
        Integer secondOperand = Integer.parseInt(callbackData.substring(callbackData.length() - 1));
        Chat chat = chats.get(chatId);

        if (chat != null) {
            chat.setSecondOperand(secondOperand);
            String result = "";

            try {
                result = "Result is: " + Chat.calculateResult(chat.getFirstOperand(), secondOperand, chat.getOperator());
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
                result = "Not correct arithmetic operation";
            }

            sendMessage.setText(result);
            sendMessage.setChatId(chatId);

            try {
                bot.execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }
}