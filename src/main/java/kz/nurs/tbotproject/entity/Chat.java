package kz.nurs.tbotproject.entity;

public class Chat {

    private Integer firstOperand;
    private Integer secondOperand;
    private String operator;
    private Long chatId;

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Integer getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(Integer firstOperand) {
        this.firstOperand = firstOperand;
    }

    public Integer getSecondOperand() {
        return secondOperand;
    }

    public void setSecondOperand(Integer secondOperand) {
        this.secondOperand = secondOperand;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public static Double calculateResult(Integer firstOperand, Integer secondOperand, String operator) {
        switch (operator) {
            case "+":
                return (double) (firstOperand + secondOperand);
            case "-":
                return (double) (firstOperand - secondOperand);
            case "*":
                return (double) (firstOperand * secondOperand);
            case "/":
                return (double) firstOperand / secondOperand;
            default:
                return 0.0;
        }
    }


}
