package kz.nurs.tbotproject.util;

import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.*;

public class KeyBoardManager {

    public enum Operand {
        FirstOperand,
        SecondOperand
    }

    public static ReplyKeyboard getNumbersKeyboard(Operand callbackOperandHint) {
        return inlineKeyboard(
                prepareButtonsList(new String[]{"1", "2", "3"}, callbackOperandHint),
                prepareButtonsList(new String[]{"4", "5", "6"}, callbackOperandHint),
                prepareButtonsList(new String[]{"7", "8", "9"}, callbackOperandHint),
                prepareButtonsList(new String[]{"0"}, callbackOperandHint)
        );
    }

    public static ReplyKeyboard getOperatorsKeyboard() {
        return inlineKeyboard(operatorButtons);
    }

    @SafeVarargs
    private static ReplyKeyboard inlineKeyboard(List<InlineKeyboardButton>... buttonList) {
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>(Arrays.asList(buttonList));
        InlineKeyboardMarkup markupKeyboard = new InlineKeyboardMarkup();
        markupKeyboard.setKeyboard(buttons);
        return markupKeyboard;
    }

    final private static List<InlineKeyboardButton> operatorButtons = new ArrayList<>(
            Arrays.asList(new InlineKeyboardButton().setText("+").setCallbackData("+"),
                    (new InlineKeyboardButton().setText("-").setCallbackData("-")),
                    (new InlineKeyboardButton().setText("*").setCallbackData("*")),
                    (new InlineKeyboardButton().setText("/").setCallbackData("/"))
            ));

    private static List<InlineKeyboardButton> prepareButtonsList(String[] numbers, Operand callbackOperandHint) {
        List<InlineKeyboardButton> list = new ArrayList<>();
        for (String st : numbers)
            list.add(new InlineKeyboardButton().setText(st).setCallbackData(callbackOperandHint.toString() + "#" + st));

        return list;
    }
}
